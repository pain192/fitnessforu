//
//  AppCoordinator.swift
//  App
//
//  Created by AnhND47.APL on 24/06/2023.
//

import UIKit
import XCoordinator

enum AppRoute: Route {
    case splash
    case home
}

class AppCoordinator: NavigationCoordinator<AppRoute> {
    init() {
        super.init(initialRoute: .home)
    }

    override func prepareTransition(for route: AppRoute) -> NavigationTransition {
        switch route {
        case .home:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: ViewController.className) as! ViewController
            viewController.initRoute(router: unownedRouter)
            return .push(viewController)
        case .splash:
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: SplashViewController.className) as! SplashViewController
            viewController.initRoute(router: unownedRouter)
            return .push(viewController)
        }
    }
}
