//
//  NSObjectExtension.swift
//  App
//
//  Created by AnhND47.APL on 24/06/2023.
//

import Foundation

extension NSObject {
    class var className: String {
        return String(describing: self)
    }
}
