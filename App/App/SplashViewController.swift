//
//  SplashViewController.swift
//  App
//
//  Created by Duy Anh Nguyen on 24/06/2023.
//

import UIKit
import XCoordinator

class SplashViewController: UIViewController {

    private var router: UnownedRouter<AppRoute>?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .darkGray
        // Do any additional setup after loading the view.
    }
    
    func initRoute(router: UnownedRouter<AppRoute>) {
        self.router = router
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    deinit {
        print("ANHND47 SplashViewController route deinited")
    }
}
