//
//  ViewController.swift
//  App
//
//  Created by AnhND47.APL on 02/06/2023.
//

import UIKit
import XCoordinator

class ViewController: UIViewController {

    private var router: UnownedRouter<AppRoute>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .orange
    }

    func initRoute(router: UnownedRouter<AppRoute>) {
        self.router = router
    }

    @IBAction func invokeButton(_ sender: UIButton) {
        router?.trigger(.splash)
    }

    deinit {
        print("ANHND47 ViewController route deinited")
    }
}

